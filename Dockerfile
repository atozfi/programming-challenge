FROM mhart/alpine-node:9.5

ENV DB_FILE /app/db/db.json

WORKDIR /app
COPY ./json-server ./
RUN yarn install
RUN npm install -g nodemon

EXPOSE 3004
# Watching file changes does not work correctly in host/container,
# hence the use of `nodemon`. Use legacy flag `-L` to detect file changes
# within mounted volume.
CMD nodemon -L --watch ${DB_FILE} --exec "yarn run start --port 3004 ${DB_FILE}"