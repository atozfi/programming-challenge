# Programming Challenge

## What to Build?

Build a frontend for browsing drink products. The user should be able to choose a category, see a list of all products with their images in the selected category, sort products based on name, preferability & price range and search their titles (case-insensitive). Not mandatory but as a bonus assignment, the user could select a product and see a product view with the selected product.

You may use a frontend framework you like (good options are [React](https://reactjs.org/), [Angular](https://angular.io/) and [Vue.js](https://vuejs.org/)).

## API

The API is running on Google Cloud Platform's Cloud Functions [serve-drinks](https://europe-west1-drinks-210704.cloudfunctions.net/serve-drinks) and serves drink products as a JSON file. There are two different drink categories:

- [Beers](https://europe-west1-drinks-210704.cloudfunctions.net/serve-drinks/beers)
- [Energy Drinks](https://europe-west1-drinks-210704.cloudfunctions.net/serve-drinks/energy-drinks)

## Product Images

Product images are stored in Google Cloud Platform Storage Bucket (https://<i></i>storage.googleapis.com/drinks-bucket/) where they are served in two different sizes:

- Original big sizes served from [/images/beers/original/](https://storage.googleapis.com/drinks-bucket/beers/original/) and [/images/energy-drinks/original/](https://storage.googleapis.com/drinks-bucket/energy-drinks/original/). So for example "Amigos" beer big size product image would be found at [/images/beers/original/amigos.jpg](https://storage.googleapis.com/drinks-bucket/beers/original/amigos.jpg).
- Thumbnail small sizes served from [/images/beers/thumb/](https://storage.googleapis.com/drinks-bucket/beers/thumb/) and [/images/energy-drinks/thumb/](https://storage.googleapis.com/drinks-bucket/energy-drinks/thumb/amigos.jpg). So for example Battery No Calories small size product image would be found at [/images/energy-drinks/thumb/battery-no-calories.jpg](https://storage.googleapis.com/drinks-bucket/energy-drinks/thumb/battery-no-calories.jpg).

Products may have one image, many images or no image. If a product does not have an image, a place holder PNG image with transparent background should be used. Similar to product images, the place holder images have two sizes, original & thumb. For energy drinks they can be found at [/images/energy-drinks/original/no-image.png](https://storage.googleapis.com/drinks-bucket/energy-drinks/original/no-image.png) and [/images/energy-drinks/thumb/no-image.png](https://storage.googleapis.com/drinks-bucket/energy-drinks/thumb/no-image.png).

## Price Range & Preferability

Price ranges and preferabilites are stored as relative values. Lower value in price range means cheaper price. Price ranges might be displayed as "€€€" based on the value. Higher value in preferability means that the product is more desireable. Preferabilities might be displayed as different amount of stars, thumbs up, etc. based on the value.

## Problems/Questions?

If something is not clear in this assignment, please contact ari.korhonen@atoz.fi.

## Delivering the Software

When you have completed the challenge, please create a GitHub/GitLab repository with all of the necessary source code + installation/run instructions. Make the repository public and send the link to it via email to ari.korhonen@atoz.fi.